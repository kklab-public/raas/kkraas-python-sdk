from typing import Any, Dict, List, Optional

from pydantic import BaseModel


class UserDatasetData(BaseModel):
    user_dataset_id: str
    name: str
    attribute_schema: Optional[Dict[str, str]] = dict()
    num_shards: int
    estimated_size: Optional[int] = None


class UserDataset:
    def __init__(self, kkraas, data_dict):
        self.kkraas = kkraas
        self.data = UserDatasetData.parse_obj(data_dict)

    def __repr__(self):
        return self.data.__repr__()

    @property
    def user_dataset_id(self):
        return self.data.user_dataset_id

    @property
    def name(self):
        return self.data.name

    @property
    def attribute_schema(self):
        return self.data.attribute_schema

    @property
    def num_shards(self):
        return self.data.num_shards

    def update(self, name: Optional[str] = None, attribute_schema: Optional[Dict[str, Any]] = None):
        updated_obj = self.kkraas.update_user_dataset(
            user_dataset_id=self.user_dataset_id,
            name=name,
            attribute_schema=attribute_schema)
        self.data = updated_obj.data
        return self

    def delete(self):
        response = self.kkraas.delete_user_dataset(
            user_dataset_id=self.user_dataset_id)
        return response

    def upsert_users(self, users: List[Dict[str, Any]]):
        response = self.kkraas.upsert_dataset_users(
            user_dataset_id=self.user_dataset_id,
            users=users)
        return response

    def delete_users(self, user_ids: List[str]):
        response = self.kkraas.delete_dataset_users(
            user_dataset_id=self.user_dataset_id,
            user_ids=user_ids)
        return response

    def delete_users_before_ts(self, ts: int):
        response = self.kkraas.delete_dataset_users_before_ts(
            user_dataset_id=self.user_dataset_id,
            ts=ts,
        )
        return response
