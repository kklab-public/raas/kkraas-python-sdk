import requests
from kkraas import util

class KKRAASException(Exception):
    pass

class KKRAASHTTPError(KKRAASException):
    request: requests.Request
    response: requests.Response
    def __init__(self, request: requests.Request, response: requests.Response):
        self.request = request
        self.response = response

    def __str__(self):
        msg = ''
        if 400 <= self.response.status_code < 500:
            msg += u'%s Client Error: %s for url: %s\n' % (self.response.status_code, self.response.reason, self.response.url)

        elif 500 <= self.response.status_code < 600:
            msg += u'%s Server Error: %s for url: %s\n' % (self.response.status_code, self.response.reason, self.response.url)

        if 400 <= self.response.status_code < 600:
            msg += '* Method: %s, Error Message: %s\n' % (self.request.method, str(self.response.content, 'utf-8'))
            msg += '* Request Content-Type: %s\n' % (str(self.request.headers["Content-Type"]))

            if self.request.method in ('POST', 'PUT', 'DELETE'):
                msg += '* Request Body: %s\n' % (util.unzip_payload(self.request.body))

        return msg
