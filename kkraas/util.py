import gzip
import json
from io import BytesIO


def json_deepcopy(d):
    return json.loads(json.dumps(d))


def zip_payload(payload: str) -> bytes:
    btsio = BytesIO()
    g = gzip.GzipFile(fileobj=btsio, mode='w')
    g.write(bytes(payload, 'utf8'))
    g.close()
    return btsio.getvalue()


def unzip_payload(data: bytes) -> str:
    btsio = BytesIO(data)
    g = gzip.GzipFile(fileobj=btsio, mode='rb')
    data = g.read()
    g.close()
    return str(data, 'utf8')