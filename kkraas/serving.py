import json
import os
import time
from typing import Any, Dict, List, Optional

import requests

from kkraas import util
from kkraas.kkraas_interface import KKRAASInterface


class KKRAASServing:
    def __init__(self, kkraas: KKRAASInterface, endpoint: str):
        self.kkraas = kkraas
        self.endpoint = endpoint

    def create_header(self, scopes: List[str], extra_payload=None):
        return self.kkraas.create_header(scopes, extra_payload)

    def post(self, url_path, data, extra_payload=None):
        url = self.endpoint + url_path

        headers = self.create_header([url_path], extra_payload)
        headers['Content-Encoding'] = 'gzip'

        post_data = util.zip_payload(json.dumps(data))

        response = requests.request("POST", url, headers=headers, data=post_data)
        response.raise_for_status()

        return response.json()

    def batch_recommend_items_to_user(self, requests: List[Dict[str, Any]], callback_url: str, payload: Optional[Dict[str, Any]] = None):
        response_dict = self.post(
            url_path='/batch-recommend-items-to-user',
            data=dict(
                requests=requests,
                callback_url=callback_url,
                payload=payload
            )
        )

        return response_dict

    def batch_recommend_items_to_item(self, requests: List[Dict[str, Any]], callback_url: str, payload: Optional[Dict[str, Any]] = None):
        response_dict = self.post(
            url_path='/batch-recommend-items-to-item',
            data=dict(
                requests=requests,
                callback_url=callback_url,
                payload=payload
            )
        )

        return response_dict

    def recommend_items_to_user(self, user_id: str, limit: int, attributes: Optional[List[str]] = None, filter_query: Optional[str] = None, generator_model_args: Optional[Dict[str, Any]] = None, ranker_model_args: Optional[Dict[str, Any]] = None):
        response_dict = self.post(
            url_path='/recommend-items-to-user',
            data=dict(
                user_id=user_id,
                attributes=attributes,
                filter_query=filter_query,
                limit=limit,
                generator_model_args=generator_model_args,
                ranker_model_args=ranker_model_args,
            )
        )

        return response_dict

    def recommend_items_to_item(self, item_id: str, limit: int, attributes: Optional[List[str]] = None, filter_query: Optional[str] = None, generator_model_args: Optional[Dict[str, Any]] = None, ranker_model_args: Optional[Dict[str, Any]] = None):
        response_dict = self.post(
            url_path='/recommend-items-to-item',
            data=dict(
                item_id=item_id,
                attributes=attributes,
                filter_query=filter_query,
                limit=limit,
                generator_model_args=generator_model_args,
                ranker_model_args=ranker_model_args,
            )
        )

        return response_dict

    def rank_items_to_user(self, user_id: str, rank_item_ids: List[str], attributes: Optional[List[str]] = None, ranker_model_args: Optional[Dict[str, Any]] = None):
        response_dict = self.post(
            url_path='/rank-items-to-user',
            data=dict(
                user_id=user_id,
                rank_item_ids=rank_item_ids,
                attributes=attributes,
                ranker_model_args=ranker_model_args,
            )
        )

        return response_dict

    def rank_items_to_item(self, item_id: str, rank_item_ids: List[str], attributes: Optional[List[str]] = None, ranker_model_args: Optional[Dict[str, Any]] = None):
        response_dict = self.post(
            url_path='/rank-items-to-item',
            data=dict(
                item_id=item_id,
                rank_item_ids=rank_item_ids,
                attributes=attributes,
                ranker_model_args=ranker_model_args,
            )
        )

        return response_dict

    def get_user_profile(self, user_id: str):
        response_dict = self.post(
            url_path='/get-user-profile',
            data=dict(
                user_id=user_id
            )
        )

        return response_dict
