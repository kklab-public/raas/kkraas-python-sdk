from abc import ABCMeta
from typing import Any, Dict, List, Optional


class KKRAASInterface(metaclass=ABCMeta):
    def create_header(self, scopes, extra_payload=None):
        raise NotImplementedError

    def get(self, url_path, params=None):
        raise NotImplementedError

    def delete(self, url_path, data):
        raise NotImplementedError

    def post(self, url_path, data):
        raise NotImplementedError

    def put(self, url_path, data):
        raise NotImplementedError

    # user dataset
    def create_user_dataset(self, name, num_shards, attribute_schema=dict()):
        raise NotImplementedError

    def update_user_dataset(self, user_dataset_id, name=None, attribute_schema=None):
        raise NotImplementedError

    def get_user_dataset(self, user_dataset_id):
        raise NotImplementedError

    def list_user_datasets(self):
        raise NotImplementedError

    def delete_user_dataset(self, user_dataset_id) -> Dict[str, Any]:
        raise NotImplementedError

    def upsert_dataset_users(self, user_dataset_id, users) -> Dict[str, Any]:
        raise NotImplementedError

    def delete_dataset_users(self, user_dataset_id, user_ids) -> Dict[str, Any]:
        raise NotImplementedError

    def delete_dataset_users_before_ts(self, user_dataset_id, ts) -> Dict[str, Any]:
        raise NotImplementedError

    # item dataset
    def create_item_dataset(self, name, num_shards, attribute_schema=dict()):
        raise NotImplementedError

    def update_item_dataset(self, item_dataset_id, name=None, attribute_schema=None):
        raise NotImplementedError

    def get_item_dataset(self, item_dataset_id):
        raise NotImplementedError

    def list_item_datasets(self):
        raise NotImplementedError

    def delete_item_dataset(self, item_dataset_id) -> Dict[str, Any]:
        raise NotImplementedError

    def upsert_dataset_items(self, item_dataset_id, items) -> Dict[str, Any]:
        raise NotImplementedError

    def delete_dataset_items(self, item_dataset_id, item_ids) -> Dict[str, Any]:
        raise NotImplementedError

    def delete_dataset_items_before_ts(self, item_dataset_id, ts) -> Dict[str, Any]:
        raise NotImplementedError

    # event dataset
    def create_event_dataset(self, name, num_shards, attribute_schema=dict()):
        raise NotImplementedError

    def update_event_dataset(self, event_dataset_id, name=None, attribute_schema=None):
        raise NotImplementedError

    def get_event_dataset(self, event_dataset_id):
        raise NotImplementedError

    def list_event_datasets(self):
        raise NotImplementedError

    def delete_event_dataset(self, event_dataset_id) -> Dict[str, Any]:
        raise NotImplementedError

    def upsert_dataset_events(self, event_dataset_id, events) -> Dict[str, Any]:
        raise NotImplementedError

    def delete_dataset_user_events(self, event_dataset_id, user_id) -> Dict[str, Any]:
        raise NotImplementedError

    def delete_dataset_events_before_ts(self, event_dataset_id, ts) -> Dict[str, Any]:
        raise NotImplementedError

    # project
    def create_project(self, name, project_type, user_dataset_id, item_dataset_id, event_dataset_id, target_event_type):
        raise NotImplementedError

    def update_project(self, project_id, name=None):
        raise NotImplementedError

    def get_project(self, project_id):
        raise NotImplementedError

    def list_projects(self):
        raise NotImplementedError

    def delete_project(self, project_id) -> Dict[str, Any]:
        raise NotImplementedError

    def get_serving(self, project_id):
        raise NotImplementedError

    # algorithm
    def create_algorithm(self, project_id, name, algorithm_type, algorithm_parameters=dict()):
        raise NotImplementedError

    def update_algorithm(self, project_id, algorithm_id, name=None, algorithm_type=None, algorithm_parameters=None):
        raise NotImplementedError

    def get_algorithm(self, project_id, algorithm_id):
        raise NotImplementedError

    def list_algorithms(self, project_id):
        raise NotImplementedError

    def delete_algorithm(self, project_id, algorithm_id) -> Dict[str, Any]:
        raise NotImplementedError

    # job
    def submit_training_job(self, project_id, name, algorithm_id):
        raise NotImplementedError

    def submit_deployment_job(self, project_id, name, generator_model_id, ranker_model_id=None, qps=None):
        raise NotImplementedError

    def get_job(self, project_id, job_id):
        raise NotImplementedError

    def list_jobs(self, project_id):
        raise NotImplementedError

    def delete_job(self, project_id, job_id) -> Dict[str, Any]:
        raise NotImplementedError

    def shutdown_deployment(self, project_id):
        raise NotImplementedError

    # model
    def update_model(self, project_id, model_id, name):
        raise NotImplementedError

    def get_model(self, project_id, model_id):
        raise NotImplementedError

    def list_models(self, project_id):
        raise NotImplementedError

    def delete_model(self, project_id, model_id) -> Dict[str, Any]:
        raise NotImplementedError
