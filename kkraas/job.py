from typing import Any, Dict, Optional

from pydantic import BaseModel


class JobData(BaseModel):
    project_id: str
    job_id: str
    job_type: str
    created_ts: int
    name: str
    job_data: Dict[str, Any]
    job_result: Optional[Dict[str, Any]] = None
    job_status: str
    retry_count: int
    start_ts: Optional[int] = None
    end_ts: Optional[int] = None


class Job:
    def __init__(self, kkraas, data_dict):
        self.kkraas = kkraas
        self.data = JobData.parse_obj(data_dict)

    def __repr__(self):
        return self.data.__repr__()

    @property
    def project_id(self):
        return self.data.project_id

    @property
    def job_id(self):
        return self.data.job_id

    @property
    def job_type(self):
        return self.data.job_type

    @property
    def created_ts(self):
        return self.data.created_ts

    @property
    def name(self):
        return self.data.name

    @property
    def job_data(self):
        return self.data.job_data

    @property
    def job_result(self):
        return self.data.job_result

    @property
    def job_status(self):
        return self.data.job_status

    @property
    def retry_count(self):
        return self.data.retry_count

    def delete(self):
        response = self.kkraas.delete_job(
            project_id=self.project_id,
            job_id=self.job_id
        )
        return response
