from typing import Any, Dict, List, Optional

from pydantic import BaseModel


class EventDatasetData(BaseModel):
    event_dataset_id: str
    name: str
    attribute_schema: Optional[Dict[str, str]] = dict()
    num_shards: int
    estimated_size: Optional[int] = None


class EventDataset:
    def __init__(self, kkraas, data_dict):
        self.kkraas = kkraas
        self.data = EventDatasetData.parse_obj(data_dict)

    def __repr__(self):
        return self.data.__repr__()

    @property
    def event_dataset_id(self):
        return self.data.event_dataset_id

    @property
    def name(self):
        return self.data.name

    @property
    def attribute_schema(self):
        return self.data.attribute_schema

    @property
    def num_shards(self):
        return self.data.num_shards

    def update(self, name: Optional[str] = None, attribute_schema: Optional[Dict[str, Any]] = None):
        updated_obj = self.kkraas.update_event_dataset(
            event_dataset_id=self.event_dataset_id,
            name=name,
            attribute_schema=attribute_schema
        )
        self.data = updated_obj.data
        return self

    def delete(self):
        response = self.kkraas.delete_event_dataset(
            event_dataset_id=self.event_dataset_id,
        )
        return response

    def upsert_events(self, events: List[Dict[str, Any]]):
        response = self.kkraas.upsert_dataset_events(
            event_dataset_id=self.event_dataset_id,
            events=events,
        )
        return response

    def delete_user_events(self, user_id: str):
        response = self.kkraas.delete_dataset_user_events(
            event_dataset_id=self.event_dataset_id,
            user_id=user_id,
        )
        return response

    def delete_events_before_ts(self, ts: int):
        response = self.kkraas.delete_dataset_events_before_ts(
            event_dataset_id=self.event_dataset_id,
            ts=ts,
        )
        return response
