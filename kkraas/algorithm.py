from typing import Any, Dict, List, Optional

from pydantic import BaseModel


class AlgorithmData(BaseModel):
    project_id: str
    algorithm_id: str
    name: str
    algorithm_type: str
    algorithm_parameters: Dict[str, Any]


class Algorithm:
    def __init__(self, kkraas, data_dict):
        self.kkraas = kkraas       
        self.data = AlgorithmData.parse_obj(data_dict)

    def __repr__(self):
        return self.data.__repr__()

    @property
    def project_id(self):
        return self.data.project_id

    @property
    def algorithm_id(self):
        return self.data.algorithm_id

    @property
    def name(self):
        return self.data.name

    @property
    def algorithm_type(self):
        return self.data.algorithm_type
    
    @property
    def algorithm_parameters(self):
        return self.data.algorithm_parameters

    def update(self, name = None, algorithm_type = None, algorithm_parameters = None):
        updated_obj = self.kkraas.update_algorithm(
            project_id=self.project_id,
            algorithm_id=self.algorithm_id,
            name=name,
            algorithm_type=algorithm_type,
            algorithm_parameters=algorithm_parameters
        )
        self.data = updated_obj.data
        return self

    def delete(self):
        response = self.kkraas.delete_algorithm(
            project_id=self.project_id,
            algorithm_id=self.algorithm_id
        )
        return response