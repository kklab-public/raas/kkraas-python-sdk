from typing import Any, Dict, List, Optional

from pydantic import BaseModel


class ProjectData(BaseModel):
    project_id: str
    name: str
    project_type: str
    user_dataset_id: str
    item_dataset_id: str
    event_dataset_id: str
    target_event_type: str
    serving_uri: str
    deployment_ts: Optional[int]
    generator_model_id: Optional[str]
    ranker_model_id: Optional[str]
    qps: Optional[int]


class Project:
    def __init__(self, kkraas, data_dict):
        self.kkraas = kkraas
        self.data = ProjectData.parse_obj(data_dict)

    def __repr__(self):
        return self.data.__repr__()

    @property
    def project_id(self):
        return self.data.project_id

    @property
    def name(self):
        return self.data.name

    @property
    def project_type(self):
        return self.data.project_type

    @property
    def user_dataset_id(self):
        return self.data.user_dataset_id

    @property
    def item_dataset_id(self):
        return self.data.item_dataset_id

    @property
    def event_dataset_id(self):
        return self.data.event_dataset_id

    @property
    def target_event_type(self):
        return self.data.target_event_type

    @property
    def serving_uri(self):
        return self.data.serving_uri

    @property
    def deployment_ts(self):
        return self.data.deployment_ts

    @property
    def generator_model_id(self):
        return self.data.generator_model_id

    @property
    def ranker_model_id(self):
        return self.data.ranker_model_id

    def update(self, name):
        updated_obj = self.kkraas.update_project(
            project_id=self.project_id,
            name=name,
        )
        self.data = updated_obj.data
        return self

    def delete(self):
        response = self.kkraas.delete_project(
            project_id=self.project_id,
        )
        return response

    def get_serving(self):
        serving = self.kkraas.get_serving(
            project_id=self.project_id,
        )
        return serving
