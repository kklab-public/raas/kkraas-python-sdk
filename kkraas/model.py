from typing import Any, Dict, List, Optional

from pydantic import BaseModel


class ModelData(BaseModel):
    project_id: str
    model_id: str
    name: str
    algorithm_type: str
    algorithm_parameters: Dict[str, Any]
    created_ts: int


class Model:
    def __init__(self, kkraas, data_dict):
        self.kkraas = kkraas       
        self.data = ModelData.parse_obj(data_dict)

    def __repr__(self):
        return self.data.__repr__()

    @property
    def project_id(self):
        return self.data.project_id

    @property
    def model_id(self):
        return self.data.model_id

    @property
    def name(self):
        return self.data.name

    @property
    def algorithm_type(self):
        return self.data.algorithm_type
    
    @property
    def algorithm_parameters(self):
        return self.data.algorithm_parameters

    @property
    def created_ts(self):
        return self.data.created_ts

    def update(self, name):
        updated_obj = self.kkraas.update_model(
            project_id=self.project_id,
            model_id=self.model_id,
            name=name
        )
        self.data = updated_obj.data
        return self

    def delete(self):
        response = self.kkraas.delete_model(
            project_id=self.project_id,
            model_id=self.model_id
        )
        return response