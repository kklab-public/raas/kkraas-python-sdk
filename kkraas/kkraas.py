import json
import os
import time
from typing import Any, Dict, List, Optional

import jwt
import requests

from kkraas import util
from kkraas.algorithm import Algorithm
from kkraas.event_dataset import EventDataset
from kkraas.impl.algorithm_impl import AlgorithmImpl
from kkraas.impl.event_dataset_impl import EventDatasetImpl
from kkraas.impl.item_dataset_impl import ItemDatasetImpl
from kkraas.impl.job_impl import JobImpl
from kkraas.impl.model_impl import ModelImpl
from kkraas.impl.project_impl import ProjectImpl
from kkraas.impl.user_dataset_impl import UserDatasetImpl
from kkraas.item_dataset import ItemDataset
from kkraas.job import Job
from kkraas.kkraas_interface import KKRAASInterface
from kkraas.model import Model
from kkraas.project import Project
from kkraas.serving import KKRAASServing
from kkraas.user_dataset import UserDataset
from kkraas.exception import KKRAASHTTPError


class KKRAAS(KKRAASInterface):
    def __init__(self, api_key: str, secret_key: str):
        self.api_key = api_key
        self.secret_key = secret_key

        self.endpoint = 'https://api.raas.kklab.com'
        if 'KKRAAS_ENDPOINT' in os.environ:
            self.endpoint = os.environ['KKRAAS_ENDPOINT']

        self.user_dataset_impl = UserDatasetImpl()
        self.item_dataset_impl = ItemDatasetImpl()
        self.event_dataset_impl = EventDatasetImpl()
        self.project_impl = ProjectImpl()
        self.algorithm_impl = AlgorithmImpl()
        self.job_impl = JobImpl()
        self.model_impl = ModelImpl()

    def create_header(self, scopes: List[str], extra_payload=None):
        payload = dict(
            scopes=scopes,
            iat=int(time.time())
        )
        if extra_payload is not None:
            payload = {**payload, **extra_payload}

        token = jwt.encode(payload, self.secret_key, algorithm='HS256')

        headers = {
            'Authorization': f'Bearer {token}',
            'APIKey': self.api_key,
            'Content-Type': 'application/json',
        }

        return headers

    def get(self, url_path, params=None):
        if params is None:
            params = dict()

        url = self.endpoint + url_path
        response = requests.request(
            'GET',
            url,
            headers=self.create_header([url_path]),
            params=params,
        )

        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            raise KKRAASHTTPError(e.request, e.response)

        return response.json()

    def delete(self, url_path, data):
        url = self.endpoint + url_path

        headers = self.create_header([url_path])
        headers['Content-Encoding'] = 'gzip'

        delete_data = util.zip_payload(json.dumps(data))

        response = requests.request("DELETE", url, headers=headers, data=delete_data)

        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            raise KKRAASHTTPError(e.request, e.response)

        return response.json()

    def post(self, url_path, data):
        url = self.endpoint + url_path

        headers = self.create_header([url_path])
        headers['Content-Encoding'] = 'gzip'

        post_data = util.zip_payload(json.dumps(data))

        response = requests.request("POST", url, headers=headers, data=post_data)

        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            raise KKRAASHTTPError(e.request, e.response)

        return response.json()

    def put(self, url_path, data):
        url = self.endpoint + url_path

        headers = self.create_header([url_path])
        headers['Content-Encoding'] = 'gzip'

        put_data = util.zip_payload(json.dumps(data))

        response = requests.request("PUT", url, headers=headers, data=put_data)

        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            raise KKRAASHTTPError(e.request, e.response)

        return response.json()

    # user dataset
    def create_user_dataset(self, name, num_shards, attribute_schema=dict()) -> UserDataset:
        return self.user_dataset_impl.create_user_dataset(self, name, num_shards, attribute_schema)

    def update_user_dataset(self, user_dataset_id, name=None, attribute_schema=None) -> UserDataset:
        return self.user_dataset_impl.update_user_dataset(self, user_dataset_id, name, attribute_schema)

    def get_user_dataset(self, user_dataset_id) -> UserDataset:
        return self.user_dataset_impl.get_user_dataset(self, user_dataset_id)

    def list_user_datasets(self) -> List[UserDataset]:
        return self.user_dataset_impl.list_user_datasets(self)

    def delete_user_dataset(self, user_dataset_id) -> Dict[str, Any]:
        return self.user_dataset_impl.delete_user_dataset(self, user_dataset_id)

    def upsert_dataset_users(self, user_dataset_id, users: List[Dict[str, Any]]) -> Dict[str, Any]:
        return self.user_dataset_impl.upsert_dataset_users(self, user_dataset_id, users)

    def delete_dataset_users(self, user_dataset_id, user_ids: List[str]) -> Dict[str, Any]:
        return self.user_dataset_impl.delete_dataset_users(self, user_dataset_id, user_ids)

    def delete_dataset_users_before_ts(self, user_dataset_id, ts) -> Dict[str, Any]:
        return self.user_dataset_impl.delete_dataset_users_before_ts(self, user_dataset_id, ts)

    # item dataset
    def create_item_dataset(self, name, num_shards, attribute_schema=dict()) -> ItemDataset:
        return self.item_dataset_impl.create_item_dataset(self, name, num_shards, attribute_schema)

    def update_item_dataset(self, item_dataset_id, name=None, attribute_schema=None) -> ItemDataset:
        return self.item_dataset_impl.update_item_dataset(self, item_dataset_id, name, attribute_schema)

    def get_item_dataset(self, item_dataset_id) -> ItemDataset:
        return self.item_dataset_impl.get_item_dataset(self, item_dataset_id)

    def list_item_datasets(self) -> List[ItemDataset]:
        return self.item_dataset_impl.list_item_datasets(self)

    def delete_item_dataset(self, item_dataset_id) -> Dict[str, Any]:
        return self.item_dataset_impl.delete_item_dataset(self, item_dataset_id)

    def upsert_dataset_items(self, item_dataset_id, items) -> Dict[str, Any]:
        return self.item_dataset_impl.upsert_dataset_items(self, item_dataset_id, items)

    def delete_dataset_items(self, item_dataset_id, item_ids) -> Dict[str, Any]:
        return self.item_dataset_impl.delete_dataset_items(self, item_dataset_id, item_ids)

    def delete_dataset_items_before_ts(self, item_dataset_id, ts) -> Dict[str, Any]:
        return self.item_dataset_impl.delete_dataset_items_before_ts(self, item_dataset_id, ts)

    # event dataset
    def create_event_dataset(self, name, num_shards, attribute_schema=dict()) -> EventDataset:
        return self.event_dataset_impl.create_event_dataset(self, name, num_shards, attribute_schema)

    def update_event_dataset(self, event_dataset_id, name=None, attribute_schema=None) -> EventDataset:
        return self.event_dataset_impl.update_event_dataset(self, event_dataset_id, name, attribute_schema)

    def get_event_dataset(self, event_dataset_id) -> EventDataset:
        return self.event_dataset_impl.get_event_dataset(self, event_dataset_id)

    def list_event_datasets(self) -> List[EventDataset]:
        return self.event_dataset_impl.list_event_datasets(self)

    def delete_event_dataset(self, event_dataset_id) -> Dict[str, Any]:
        return self.event_dataset_impl.delete_event_dataset(self, event_dataset_id)

    def upsert_dataset_events(self, event_dataset_id, events) -> Dict[str, Any]:
        return self.event_dataset_impl.upsert_dataset_events(self, event_dataset_id, events)

    def delete_dataset_user_events(self, event_dataset_id, user_id) -> Dict[str, Any]:
        return self.event_dataset_impl.delete_dataset_user_events(self, event_dataset_id, user_id)

    def delete_dataset_events_before_ts(self, event_dataset_id, ts) -> Dict[str, Any]:
        return self.event_dataset_impl.delete_dataset_events_before_ts(self, event_dataset_id, ts)

    # project
    def create_project(self, name, project_type, user_dataset_id, item_dataset_id, event_dataset_id, target_event_type) -> Project:
        return self.project_impl.create_project(self, name, project_type, user_dataset_id, item_dataset_id, event_dataset_id, target_event_type)

    def update_project(self, project_id, name=None) -> Project:
        return self.project_impl.update_project(self, project_id, name)

    def get_project(self, project_id) -> Project:
        return self.project_impl.get_project(self, project_id)

    def list_projects(self) -> List[Project]:
        return self.project_impl.list_projects(self)

    def delete_project(self, project_id) -> Dict[str, Any]:
        return self.project_impl.delete_project(self, project_id)

    def get_serving(self, project_id) -> KKRAASServing:
        return self.project_impl.get_serving(self, project_id)

    # algorithm
    def create_algorithm(self, project_id, name, algorithm_type, algorithm_parameters=dict()) -> Algorithm:
        return self.algorithm_impl.create_algorithm(self, project_id, name, algorithm_type, algorithm_parameters)

    def update_algorithm(self, project_id, algorithm_id, name=None, algorithm_type=None, algorithm_parameters=None) -> Algorithm:
        return self.algorithm_impl.update_algorithm(self, project_id, algorithm_id, name, algorithm_type, algorithm_parameters)

    def get_algorithm(self, project_id, algorithm_id) -> Algorithm:
        return self.algorithm_impl.get_algorithm(self, project_id, algorithm_id)

    def list_algorithms(self, project_id) -> List[Algorithm]:
        return self.algorithm_impl.list_algorithms(self, project_id)

    def delete_algorithm(self, project_id, algorithm_id) -> Dict[str, Any]:
        return self.algorithm_impl.delete_algorithm(self, project_id, algorithm_id)

    # job
    def submit_training_job(self, project_id, name, algorithm_id) -> Dict[str, Any]:
        return self.job_impl.submit_training_job(self, project_id, name, algorithm_id)

    def submit_deployment_job(self, project_id, name, generator_model_id, ranker_model_id=None, qps=None) -> Dict[str, Any]:
        return self.job_impl.submit_deployment_job(self, project_id, name, generator_model_id, ranker_model_id, qps)

    def get_job(self, project_id, job_id) -> Job:
        return self.job_impl.get_job(self, project_id, job_id)

    def list_jobs(self, project_id) -> List[Job]:
        return self.job_impl.list_jobs(self, project_id)

    def delete_job(self, project_id, job_id) -> Dict[str, Any]:
        return self.job_impl.delete_job(self, project_id, job_id)

    def shutdown_deployment(self, project_id) -> Dict[str, Any]:
        return self.job_impl.shutdown_deployment(self, project_id)

    # model
    def update_model(self, project_id, model_id, name) -> Model:
        return self.model_impl.update_model(self, project_id, model_id, name)

    def get_model(self, project_id, model_id) -> Model:
        return self.model_impl.get_model(self, project_id, model_id)

    def list_models(self, project_id) -> List[Model]:
        return self.model_impl.list_models(self, project_id)

    def delete_model(self, project_id, model_id) -> Dict[str, Any]:
        return self.model_impl.delete_model(self, project_id, model_id)
