from typing import Any, Dict, List, Optional

from kkraas.kkraas_interface import KKRAASInterface
from kkraas.model import Model


class ModelImpl:

    def update_model(self, kkraas: KKRAASInterface, project_id: str, model_id: str, name: str) -> Model:
        data = {
            'project_id': project_id,
            'model_id': model_id,
        }
        if name:
            data['name'] = name
        response_dict = kkraas.put(
            url_path='/update-model',
            data=data,
        )

        model = Model(
            kkraas=kkraas,
            data_dict=response_dict,
        )
        return model

    def get_model(self, kkraas: KKRAASInterface, project_id: str, model_id: str) -> Model:
        response_dict = kkraas.get(
            url_path='/get-model',
            params=dict(
                project_id=project_id,
                model_id=model_id,
            )
        )

        model = Model(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return model

    def list_models(self, kkraas: KKRAASInterface,  project_id: str) -> List[Model]:
        response_dict = kkraas.get(
            url_path='/list-models',
            params=dict(
                project_id=project_id,
            )
        )

        model_list = []
        for data_dict in response_dict['models']:
            model = Model(
                kkraas=kkraas,
                data_dict=data_dict,
            )
            model_list.append(model)

        return model_list

    def delete_model(self, kkraas: KKRAASInterface, project_id: str, model_id: str) -> Dict[str, Any]:
        response_dict = kkraas.delete(
            url_path='/delete-model',
            data=dict(
                project_id=project_id,
                model_id=model_id,
            )
        )

        return response_dict

