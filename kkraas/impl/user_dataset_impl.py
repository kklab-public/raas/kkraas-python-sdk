from typing import Any, Dict, List, Optional

from kkraas.kkraas_interface import KKRAASInterface
from kkraas.user_dataset import UserDataset


class UserDatasetImpl:

    def create_user_dataset(self, kkraas: KKRAASInterface, name: str, num_shards: int, attribute_schema: Dict[str, Any] = dict()) -> UserDataset:
        response_dict = kkraas.post(
            url_path='/create-user-dataset',
            data=dict(
                name=name,
                num_shards=num_shards,
                attribute_schema=attribute_schema
            )
        )

        user_dataset = UserDataset(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return user_dataset

    def update_user_dataset(self, kkraas: KKRAASInterface, user_dataset_id: str, name: Optional[str] = None, attribute_schema: Optional[Dict[str, Any]] = None) -> UserDataset:
        data = {
            'user_dataset_id': user_dataset_id
        }
        if name:
            data['name'] = name
        if attribute_schema:
            data['attribute_schema'] = attribute_schema
        response_dict = kkraas.put(
            url_path='/update-user-dataset',
            data=data,
        )

        user_dataset = UserDataset(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return user_dataset

    def get_user_dataset(self, kkraas: KKRAASInterface, user_dataset_id: str) -> UserDataset:
        response_dict = kkraas.get(
            url_path='/get-user-dataset',
            params=dict(
                user_dataset_id=user_dataset_id,
            )
        )

        user_dataset = UserDataset(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return user_dataset

    def list_user_datasets(self, kkraas: KKRAASInterface) -> List[UserDataset]:
        response_dict = kkraas.get(
            url_path='/list-user-datasets',
        )

        dataset_list = []
        for data_dict in response_dict['user_datasets']:
            user_dataset = UserDataset(
                kkraas=kkraas,
                data_dict=data_dict,
            )
            dataset_list.append(user_dataset)

        return dataset_list

    def delete_user_dataset(self, kkraas: KKRAASInterface, user_dataset_id: str) -> Dict[str, Any]:
        response_dict = kkraas.delete(
            url_path='/delete-user-dataset',
            data=dict(
                user_dataset_id=user_dataset_id,
            )
        )

        return response_dict

    def upsert_dataset_users(self, kkraas: KKRAASInterface, user_dataset_id: str, users: List[Dict[str, Any]]) -> Dict[str, Any]:
        response_dict = kkraas.post(
            url_path='/upsert-dataset-users',
            data=dict(
                user_dataset_id=user_dataset_id,
                users=users
            )
        )

        return response_dict

    def delete_dataset_users(self, kkraas: KKRAASInterface, user_dataset_id: str, user_ids: List[str]) -> Dict[str, Any]:
        response_dict = kkraas.delete(
            url_path='/delete-dataset-users',
            data=dict(
                user_dataset_id=user_dataset_id,
                user_ids=user_ids
            )
        )

        return response_dict

    def delete_dataset_users_before_ts(self, kkraas: KKRAASInterface, user_dataset_id: str, ts: int) -> Dict[str, Any]:
        response_dict = kkraas.delete(
            url_path='/delete-dataset-users-before-ts',
            data=dict(
                user_dataset_id=user_dataset_id,
                ts=ts,
            )
        )

        return response_dict
