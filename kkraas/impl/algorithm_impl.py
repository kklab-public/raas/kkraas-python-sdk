from typing import Any, Dict, List, Optional

from kkraas.kkraas_interface import KKRAASInterface
from kkraas.algorithm import Algorithm


class AlgorithmImpl:
    
    def create_algorithm(self, kkraas: KKRAASInterface, project_id: str, name: str, algorithm_type: str, algorithm_parameters: Dict[str, Any] = dict()) -> Algorithm:
        response_dict = kkraas.post(
            url_path='/create-algorithm',
            data=dict(
                project_id=project_id,
                name=name,
                algorithm_type=algorithm_type,
                algorithm_parameters=algorithm_parameters
            )
        )
        
        algorithm = Algorithm(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return algorithm

    def update_algorithm(self, kkraas: KKRAASInterface, project_id: str, algorithm_id: str, name: Optional[str] = None, algorithm_type: Optional[str] = None, algorithm_parameters: Optional[Dict[str, Any]] = None) -> Algorithm:
        data = {
            'project_id': project_id,
            'algorithm_id': algorithm_id,
        }
        if name:
            data['name'] = name
        if algorithm_type:
            data['algorithm_type'] = algorithm_type
        if algorithm_parameters:
            data['algorithm_parameters'] = algorithm_parameters
        response_dict = kkraas.put(
            url_path='/update-algorithm',
            data=data,
        )

        algorithm = Algorithm(
            kkraas=kkraas,
            data_dict=response_dict,
        )
        return algorithm

    def get_algorithm(self, kkraas: KKRAASInterface, project_id: str, algorithm_id: str) -> Algorithm:
        response_dict = kkraas.get(
            url_path='/get-algorithm',
            params=dict(
                project_id=project_id,
                algorithm_id=algorithm_id,
            )
        )

        algorithm = Algorithm(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return algorithm

    def list_algorithms(self, kkraas: KKRAASInterface,  project_id: str) -> List[Algorithm]:
        response_dict = kkraas.get(
            url_path='/list-algorithms',
            params=dict(
                project_id=project_id,
            )
        )

        algorithm_list = []
        for data_dict in response_dict['algorithms']:
            algorithm = Algorithm(
                kkraas=kkraas,
                data_dict=data_dict,
            )
            algorithm_list.append(algorithm)

        return algorithm_list

    def delete_algorithm(self, kkraas: KKRAASInterface, project_id: str, algorithm_id: str) -> Dict[str, Any]:
        response_dict = kkraas.delete(
            url_path='/delete-algorithm',
            data=dict(
                project_id=project_id,
                algorithm_id=algorithm_id,
            )
        )

        return response_dict

