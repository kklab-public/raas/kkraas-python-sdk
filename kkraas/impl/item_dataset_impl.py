from typing import Any, Dict, List, Optional

from kkraas.kkraas_interface import KKRAASInterface
from kkraas.item_dataset import ItemDataset


class ItemDatasetImpl:

    def create_item_dataset(self, kkraas: KKRAASInterface, name: str, num_shards: int, attribute_schema: Dict[str, Any] = dict()) -> ItemDataset:
        response_dict = kkraas.post(
            url_path='/create-item-dataset',
            data=dict(
                name=name,
                num_shards=num_shards,
                attribute_schema=attribute_schema,
            )
        )

        item_dataset = ItemDataset(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return item_dataset

    def update_item_dataset(self, kkraas: KKRAASInterface, item_dataset_id: str, name: Optional[str] = None, attribute_schema: Optional[Dict[str, Any]] = None) -> ItemDataset:
        data = {'item_dataset_id': item_dataset_id}
        if name:
            data['name'] = name
        if attribute_schema:
            data['attribute_schema'] = attribute_schema
        response_dict = kkraas.put(
            url_path='/update-item-dataset',
            data=data,
        )

        item_dataset = ItemDataset(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return item_dataset

    def get_item_dataset(self, kkraas: KKRAASInterface, item_dataset_id: str) -> ItemDataset:
        response_dict = kkraas.get(
            url_path='/get-item-dataset',
            params=dict(
                item_dataset_id=item_dataset_id,
            )
        )

        item_dataset = ItemDataset(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return item_dataset

    def list_item_datasets(self, kkraas: KKRAASInterface) -> List[ItemDataset]:
        response_dict = kkraas.get(
            url_path='/list-item-datasets',
        )

        dataset_list = []
        for data_dict in response_dict['item_datasets']:
            item_dataset = ItemDataset(
                kkraas=kkraas,
                data_dict=data_dict,
            )
            dataset_list.append(item_dataset)

        return dataset_list

    def delete_item_dataset(self, kkraas: KKRAASInterface, item_dataset_id: str) -> Dict[str, Any]:
        response_dict = kkraas.delete(
            url_path='/delete-item-dataset',
            data=dict(
                item_dataset_id=item_dataset_id,
            )
        )

        return response_dict

    def upsert_dataset_items(self, kkraas: KKRAASInterface, item_dataset_id: str, items: List[Dict[str, Any]]) -> Dict[str, Any]:
        response_dict = kkraas.post(
            url_path='/upsert-dataset-items',
            data=dict(
                item_dataset_id=item_dataset_id,
                items=items
            )
        )

        return response_dict

    def delete_dataset_items(self, kkraas: KKRAASInterface, item_dataset_id: str, item_ids: List[str]) -> Dict[str, Any]:
        response_dict = kkraas.delete(
            url_path='/delete-dataset-items',
            data=dict(
                item_dataset_id=item_dataset_id,
                item_ids=item_ids
            )
        )

        return response_dict

    def delete_dataset_items_before_ts(self, kkraas: KKRAASInterface, item_dataset_id: str, ts: int) -> Dict[str, Any]:
        response_dict = kkraas.delete(
            url_path='/delete-dataset-items-before-ts',
            data=dict(
                item_dataset_id=item_dataset_id,
                ts=ts
            )
        )

        return response_dict
