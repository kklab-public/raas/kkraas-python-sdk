from typing import Any, Dict, List, Optional

from kkraas.job import Job
from kkraas.kkraas_interface import KKRAASInterface


class JobImpl:

    def submit_training_job(self, kkraas: KKRAASInterface, project_id: str, name: str, algorithm_id: str) -> Dict[str, Any]:
        response_dict = kkraas.post(
            url_path='/submit-training-job',
            data=dict(
                project_id=project_id,
                algorithm_id=algorithm_id,
                name=name
            )
        )

        return response_dict

    def submit_deployment_job(
            self,
            kkraas: KKRAASInterface,
            project_id: str,
            name: str,
            generator_model_id: str,
            ranker_model_id: Optional[str] = None,
            qps: Optional[int] = 1,) -> Dict[str, Any]:
        response_dict = kkraas.post(
            url_path='/submit-deployment-job',
            data=dict(
                project_id=project_id,
                generator_model_id=generator_model_id,
                ranker_model_id=ranker_model_id,
                name=name,
                qps=qps,
            )
        )

        return response_dict

    def get_job(self, kkraas: KKRAASInterface, project_id: str, job_id: str) -> Job:
        response_dict = kkraas.get(
            url_path='/get-job',
            params=dict(
                project_id=project_id,
                job_id=job_id,
            )
        )

        job = Job(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return job

    def list_jobs(self, kkraas: KKRAASInterface, project_id: str) -> List[Job]:
        response_dict = kkraas.get(
            url_path='/list-jobs',
            params=dict(
                project_id=project_id,
            )
        )

        job_list = []
        for data_dict in response_dict['jobs']:
            job = Job(
                kkraas=kkraas,
                data_dict=data_dict,
            )
            job_list.append(job)

        return job_list

    def delete_job(self, kkraas: KKRAASInterface, project_id: str, job_id: str) -> Dict[str, Any]:
        response_dict = kkraas.delete(
            url_path='/delete-job',
            data=dict(
                project_id=project_id,
                job_id=job_id,
            )
        )

        return response_dict

    def shutdown_deployment(self, kkraas: KKRAASInterface, project_id: str) -> Dict[str, Any]:
        response_dict = kkraas.delete(
            url_path='/shutdown-deployment',
            data=dict(
                project_id=project_id,
            )
        )

        return response_dict
