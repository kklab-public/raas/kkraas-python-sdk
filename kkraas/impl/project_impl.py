from typing import Any, Dict, List, Optional

from kkraas.kkraas_interface import KKRAASInterface
from kkraas.project import Project
from kkraas.serving import KKRAASServing


class ProjectImpl:

    def create_project(self, kkraas: KKRAASInterface, name: str, project_type: str, user_dataset_id: str, item_dataset_id: str, event_dataset_id: str, target_event_type: str) -> Project:
        response_dict = kkraas.post(
            url_path='/create-project',
            data=dict(
                name=name,
                project_type=project_type,
                user_dataset_id=user_dataset_id,
                item_dataset_id=item_dataset_id,
                event_dataset_id=event_dataset_id,
                target_event_type=target_event_type
            )
        )

        project = Project(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return project

    def update_project(self, kkraas: KKRAASInterface, project_id: str, name: str) -> Project:
        data = {'project_id': project_id}
        if name:
            data['name'] = name
        response_dict = kkraas.put(
            url_path='/update-project',
            data=data,
        )

        project = Project(
            kkraas=kkraas,
            data_dict=response_dict,
        )
        return project

    def get_project(self, kkraas: KKRAASInterface, project_id: str) -> Project:
        response_dict = kkraas.get(
            url_path='/get-project',
            params=dict(
                project_id=project_id,
            )
        )

        project = Project(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return project

    def list_projects(self, kkraas: KKRAASInterface) -> List[Project]:
        response_dict = kkraas.get(
            url_path='/list-projects',
        )

        project_list = []
        for data_dict in response_dict['projects']:
            project = Project(
                kkraas=kkraas,
                data_dict=data_dict,
            )
            project_list.append(project)

        return project_list

    def delete_project(self, kkraas: KKRAASInterface, project_id: str) -> Dict[str, Any]:
        response_dict = kkraas.delete(
            url_path='/delete-project',
            data=dict(
                project_id=project_id,
            )
        )

        return response_dict

    def get_serving(self, kkraas: KKRAASInterface, project_id: str) -> KKRAASServing:
        project = self.get_project(kkraas, project_id)
        return KKRAASServing(kkraas, project.data.serving_uri)
