from typing import Any, Dict, List, Optional

from kkraas.kkraas_interface import KKRAASInterface
from kkraas.event_dataset import EventDataset


class EventDatasetImpl:

    def create_event_dataset(self, kkraas: KKRAASInterface, name: str, num_shards: int, attribute_schema: Dict[str, Any] = dict()) -> EventDataset:
        response_dict = kkraas.post(
            url_path='/create-event-dataset',
            data=dict(
                name=name,
                num_shards=num_shards,
                attribute_schema=attribute_schema
            )
        )

        event_dataset = EventDataset(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return event_dataset

    def update_event_dataset(self, kkraas: KKRAASInterface, event_dataset_id: str, name: Optional[str] = None, attribute_schema: Optional[Dict[str, Any]] = None) -> EventDataset:
        data = {'event_dataset_id': event_dataset_id}
        if name:
            data['name'] = name
        if attribute_schema:
            data['attribute_schema'] = attribute_schema
        response_dict = kkraas.put(
            url_path='/update-event-dataset',
            data=data
        )

        event_dataset = EventDataset(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return event_dataset

    def get_event_dataset(self, kkraas: KKRAASInterface, event_dataset_id: str) -> EventDataset:
        response_dict = kkraas.get(
            url_path='/get-event-dataset',
            params=dict(
                event_dataset_id=event_dataset_id,
            )
        )

        event_dataset = EventDataset(
            kkraas=kkraas,
            data_dict=response_dict,
        )

        return event_dataset

    def list_event_datasets(self, kkraas: KKRAASInterface) -> List[EventDataset]:
        response_dict = kkraas.get(
            url_path='/list-event-datasets',
        )

        dataset_list = []
        for data_dict in response_dict['event_datasets']:
            event_dataset = EventDataset(
                kkraas=kkraas,
                data_dict=data_dict,
            )
            dataset_list.append(event_dataset)

        return dataset_list

    def delete_event_dataset(self, kkraas: KKRAASInterface, event_dataset_id: str) -> Dict[str, Any]:
        response_dict = kkraas.delete(
            url_path='/delete-event-dataset',
            data=dict(
                event_dataset_id=event_dataset_id,
            )
        )

        return response_dict

    def upsert_dataset_events(self, kkraas: KKRAASInterface, event_dataset_id: str, events: List[Dict[str, Any]]) -> Dict[str, Any]:
        response_dict = kkraas.post(
            url_path='/upsert-dataset-events',
            data=dict(
                event_dataset_id=event_dataset_id,
                events=events
            )
        )

        return response_dict

    def delete_dataset_user_events(self, kkraas: KKRAASInterface, event_dataset_id: str, user_id: str) -> Dict[str, Any]:
        response_dict = kkraas.delete(
            url_path='/delete-dataset-user-events',
            data=dict(
                event_dataset_id=event_dataset_id,
                user_id=user_id
            )
        )

        return response_dict

    def delete_dataset_events_before_ts(self, kkraas: KKRAASInterface, event_dataset_id: str, ts: int) -> Dict[str, Any]:
        response_dict = kkraas.delete(
            url_path='/delete-dataset-events-before-ts',
            data=dict(
                event_dataset_id=event_dataset_id,
                ts=ts
            )
        )

        return response_dict
