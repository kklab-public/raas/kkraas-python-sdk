from typing import Any, Dict, List, Optional

from pydantic import BaseModel


class ItemDatasetData(BaseModel):
    item_dataset_id: str
    name: str
    attribute_schema: Optional[Dict[str, str]] = dict()
    num_shards: int
    estimated_size: Optional[int] = None


class ItemDataset:
    def __init__(self, kkraas, data_dict):
        self.kkraas = kkraas
        self.data = ItemDatasetData.parse_obj(data_dict)

    def __repr__(self):
        return self.data.__repr__()

    @property
    def item_dataset_id(self):
        return self.data.item_dataset_id

    @property
    def name(self):
        return self.data.name

    @property
    def attribute_schema(self):
        return self.data.attribute_schema

    @property
    def num_shards(self):
        return self.data.num_shards

    def update(self, name: Optional[str] = None, attribute_schema: Optional[Dict[str, Any]] = None):
        updated_obj = self.kkraas.update_item_dataset(
            item_dataset_id=self.item_dataset_id,
            name=name,
            attribute_schema=attribute_schema
        )
        self.data = updated_obj.data
        return self

    def delete(self):
        response = self.kkraas.delete_item_dataset(
            item_dataset_id=self.item_dataset_id,
        )
        return response

    def upsert_items(self, items: List[Dict[str, Any]]):
        response = self.kkraas.upsert_dataset_items(
            item_dataset_id=self.item_dataset_id,
            items=items,
        )
        return response

    def delete_items(self, item_ids: List[str]):
        response = self.kkraas.delete_dataset_items(
            item_dataset_id=self.item_dataset_id,
            item_ids=item_ids,
        )
        return response

    def delete_items_before_ts(self, ts: int):
        response = self.kkraas.delete_dataset_items_before_ts(
            item_dataset_id=self.item_dataset_id,
            ts=ts,
        )
        return response
