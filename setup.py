"""A setuptools based setup module.

See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
import setuptools


def get_version():
    with open('VERSION', mode='r') as fin:
        lines = fin.readlines()
        for line in lines:
            if line[0] != '#':
                version = line.strip()
                return version


if __name__ == '__main__':
    setuptools.setup(
        name='kkraas-python-sdk',

        # Versions should comply with PEP440.  For a discussion on single-sourcing
        # the version across setup.py and the project code, see
        # https://packaging.python.org/en/latest/single_source_version.html
        version=get_version(),

        description='KKRAAS Python SDK',
        long_description='KKRAAS Python SDK',

        # The project's main homepage.
        url='https://gitlab.com/kklab-public/raas/kkraas-python-sdk',

        # Author details
        author='KKLAB Inc.',
        author_email='kkraas@kklab.com',

        # Choose your license
        license='MIT',

        # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
        classifiers=[
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: MIT License",
        ],

        # What does your project relate to?
        keywords='kkraas',

        # You can just specify the packages manually here if your project is
        # simple. Or you can use find_packages().
        packages=setuptools.find_packages(exclude=[
        ]),

        # List run-time dependencies here.  These will be installed by pip when
        # your project is installed. For an analysis of "install_requires" vs pip's
        # requirements files see:
        # https://packaging.python.org/en/latest/requirements.html
        install_requires=[
            'pydantic>=1.8.2',
            'PyJWT>=2.1.0',
            'requests>=2.25.1',
        ],

        # List additional groups of dependencies here (e.g. development
        # dependencies). You can install these using the following syntax,
        # for example:
        # $ pip install -e .[dev,test]
        extras_require={
            'dev': [
            ],
        },

        # If there are data files included in your packages that need to be
        # installed, specify them here.  If using Python 2.6 or less, then these
        # have to be included in MANIFEST.in as well.
        package_data={},

        # Although 'package_data' is the preferred approach, in some case you may
        # need to place data files outside of your packages. See:
        # http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files # noqa
        # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
        # data_files=[('my_data', ['data/data_file.txt'])],\

        # To provide executable scripts, use entry points in preference to the
        # "scripts" keyword. Entry points provide cross-platform support and allow
        # pip to create the appropriate form of executable for the target platform.
        # entry_points={
        #     'console_scripts': [
        #         'sample=sample:main',
        #     ],
        # },
    )
